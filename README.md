# Blank React Native Project

This is a base project for React Native. It brings you a good basic architecture and the use of React Navigation.

## Installation

```
npm install
```

Change all "blank_react_native_project" occurencies with your project name.
Change all "blank_react_native_project" files in IOS folder with your project name.
If you are facing some issues, it may be more simple to get the `android` and `ios` folder from a new project.

To test the project:

```
npx react-native run-android
```

## Build the debug APK with Docker

```
npm install
docker-compose up --build
```

The built APK is in `docker/out`
