export const mainTheme = {
  primaryColor: '#fff',
  secondaryColor: '#000',
  defaultPadding: 16,
  defaultMargin: 16,
};
