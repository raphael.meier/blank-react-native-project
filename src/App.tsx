import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import HomeScreen from 'screens/home';
import DetailScreen from 'screens/detail';

import {mainTheme} from 'themes/main_theme';

const MainStack = createNativeStackNavigator<MainStackParamList>();

const App = () => {
  return (
    <NavigationContainer>
      <SafeAreaView style={styles.container}>
        <MainStack.Navigator screenOptions={{contentStyle: styles.screen}}>
          <MainStack.Screen
            name="Home"
            component={HomeScreen}
            options={{headerShown: false}}
          />
          <MainStack.Screen name="Detail" component={DetailScreen} />
        </MainStack.Navigator>
      </SafeAreaView>
    </NavigationContainer>
  );
};
export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: mainTheme.defaultPadding,
    backgroundColor: mainTheme.primaryColor,
  },
  screen: {flex: 1, backgroundColor: 'transparent'},
});
