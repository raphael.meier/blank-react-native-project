import React, {FC} from 'react';
import {StyleSheet, Text, View} from 'react-native';

import {RouteProp} from '@react-navigation/native-stack';

interface DetaiScreenProps {
  route: RouteProp<MainStackParamList, 'Detail'>;
}

const DetailScreen: FC<DetaiScreenProps> = ({route}) => {
  return (
    <View style={styles.detailScreen}>
      <Text>{route.params.id}</Text>
    </View>
  );
};
export default DetailScreen;

const styles = StyleSheet.create({
  detailScreen: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
});
