import React, {FC} from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';

import {NativeStackNavigationProp} from '@react-navigation/native-stack';

interface HomeScreenProps {
  navigation: NativeStackNavigationProp<MainStackParamList, 'Home'>;
}

const HomeScreen: FC<HomeScreenProps> = ({navigation}) => {
  const goToDetail = () => {
    navigation.navigate('Detail', {
      id: 1,
    });
  };

  return (
    <View style={styles.homeScreen}>
      <Text>Hello World</Text>
      <Button title="GO" onPress={goToDetail} />
    </View>
  );
};
export default HomeScreen;

const styles = StyleSheet.create({
  homeScreen: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
});
